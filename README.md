## File Extension Deleter

## 📜 Description
I'm starting this project to practice some Python and to help my girlfriend.
She was having a hard time deleting all files with an specific extension from her folders.
It were some IMG*.mov files inside her backup from her gallery.
The ideia is to help her delete all these files in a faster way.

## 💻 Requirements
You will need:
* Python;

## ☕ Using Project
To use this project, first you need to copy this script somewhere.
I used GIT BASH to run it.

To use this script, run with the following format
FileExtensionDelete.py '<File Path>' <File Extension> <File Begining>
Example: python ./FileExtensionDelete.py \'C:\\Users\\yourUser\\Desktop\\Projects\\Path To Delete\' mov IMG

The path can be in both ways: (both inside the quotation marks '')
C:\\Users\\yourUser\\Desktop\\Projects\\Path To Delete 
Or
/c/Users/yourUser/Desktop/Projects/Path To Delete

In this example, the script will delete all files with the extension '.mov' and begin with 'IMG' in the specified path
All IMG*.mov files will be deleted
If you want to delete ALL files with an extension, use '+*' as the file begining
If the path does not exist, the script will exit


## 🔧 Fixes and new features
I just made a simple version that can be improved.
- Delete recursively inside a folder;
- ...

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)