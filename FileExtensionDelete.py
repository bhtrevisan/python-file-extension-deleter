# File Extension Delete

import sys, os

if len(sys.argv) < 4:
    print("There is at least one missing argument\n")
    print("To use this script, run with the following format")
    print("FileExtensionDelete.py '<File Path>' <File Extension> <File Begining>\n")
    print("Example: python ./FileExtensionDelete.py \'C:\\Users\\yourUser\\Desktop\\Projects\\Path To Delete\' mov IMG")
    print("\nThe path can be in both ways: (both inside the quotation marks '')")
    print("C:\\Users\\yourUser\\Desktop\\Projects\\Path To Delete\n Or")
    print("/c/Users/yourUser/Desktop/Projects/Path To Delete\n")
    print("With this example, the script will delete all filed with the extension '.mov' and begin with 'IMG' in the specified path")
    print("All IMG*.mov files will be deleted\n")
    print("If you want to delete ALL files with an extension, use '+*' as the file begining")
    print("If the path does not exist, the script will exit")
    exit()

print (sys.argv)

filePath = sys.argv[1]
fileExtension = sys.argv[2]
fileBegining = sys.argv[3]

print("\nFile Path to be deleted: ", filePath)
print("File Extension to be deleted: ", fileExtension)
print("File Begining to be deleted: ", fileBegining + '\n')

if os.path.exists(filePath):
    print("File Path Found")
    # print(os.listdir(filePath))
    print("Start checking all files in the path...\n")
    for item in os.listdir(filePath):
        if (item.endswith("." + fileExtension) and item.startswith(fileBegining)) or (item.endswith("." + fileExtension) and fileBegining == "+*"):
            print(item)
            os.remove(filePath + "\\" + item)
            print("Item Deleted")
    print("\nAll Items Checked. Exiting")
else:
    print("File Path Does Not Exist")
    print("Exiting")
exit()
